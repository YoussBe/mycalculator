import 'package:flutter/material.dart';
import 'package:mycalculator/app/screens/history/history_screen.dart';

import 'app/screens/home/home_screen.dart';
import 'app/screens/splash/splash_screen.dart';

const kMainRoute = '/';
const kHomeRoute = '/home';
const kHistoryRoute = '/History';

final Map<String, WidgetBuilder> kRoutes = {
  kMainRoute: (_) => SplashScreen(),
  kHomeRoute: (_) => HomeScreen(),
  kHistoryRoute: (_) => HistoryScreen(),
};
