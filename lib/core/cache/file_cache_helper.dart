import 'dart:io';

import 'package:path_provider/path_provider.dart';

class FileCacheHelper {
  Future<String> read(String key) async {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/icalculator_cache_$key');
    return await file.readAsString();
  }

  Future<File> save(String key, String data) async {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/icalculator_cache_$key');
    return await file.writeAsString(data);
  }

  Future remove(String key) async {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/icalculator_cache_$key');
    return await file.delete();
  }
}
