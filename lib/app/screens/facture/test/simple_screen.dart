import 'package:camera/camera.dart';
import 'package:flutter/material.dart';

class SimpleCameraScreen extends StatefulWidget {
  SimpleCameraScreen({Key key}) : super(key: key);

  @override
  _SimpleCameraScreenState createState() => _SimpleCameraScreenState();
}

class _SimpleCameraScreenState extends State<SimpleCameraScreen> {
  CameraController controller;
  List<CameraDescription> cameras;

  Future initCameras() async {
    cameras = await availableCameras();
    controller = CameraController(cameras[0], ResolutionPreset.medium);
    controller.initialize().then((_) {
      if (!mounted) {
        setState(() {});

        return;
      }
      setState(() {});
    });
  }

  @override
  void initState() {
    initCameras();
    super.initState();
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: controller == null
          ? Container()
          : Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(50))),
                  elevation: 20,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: AspectRatio(
                        aspectRatio: 1, child: CameraPreview(controller)),
                  )),
            ),
    );
  }
}
