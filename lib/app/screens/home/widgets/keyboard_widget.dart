import 'package:flutter/material.dart';
import 'package:mycalculator/app/screens/home/widgets/Custom_button_widget.dart';

class Keyboard extends StatelessWidget {
  const Keyboard({
    this.function,
    Key key,
  }) : super(key: key);
  final Function function;

  void testFuntion(String symbol) {
    print(symbol);
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            CustomButton(
              content: 'C',
              onTapButton: () => function('C'),
            ),
            CustomButton(
              content: '(',
              onTapButton: () => function('('),
            ),
            CustomButton(
              content: ')',
            ),
            CustomButton(
              content: '/',
            ),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            CustomButton(
              content: '7',
              isDark: true,
            ),
            CustomButton(
              content: '8',
              isDark: true,
            ),
            CustomButton(
              content: '9',
              isDark: true,
            ),
            CustomButton(
              content: 'X',
            ),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            CustomButton(
              content: '4',
              isDark: true,
            ),
            CustomButton(
              content: '5',
              isDark: true,
            ),
            CustomButton(
              content: '6',
              isDark: true,
            ),
            CustomButton(
              content: '-',
            ),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            CustomButton(
              content: '1',
              isDark: true,
            ),
            CustomButton(
              content: '2',
              isDark: true,
            ),
            CustomButton(
              content: '3',
              isDark: true,
            ),
            CustomButton(
              content: '+',
            ),
          ]),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            CustomButton(
              content: '0',
              isBig: true,
              isDark: true,
            ),
            CustomButton(
              content: '.',
              isDark: true,
            ),
            CustomButton(
              content: '=',
            ),
          ])
        ],
      ),
    );
  }
}
