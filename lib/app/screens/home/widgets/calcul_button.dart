import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class CalculButton extends StatelessWidget {
  const CalculButton({
    Key key,
    this.content,
    this.isDark = false,
    this.doesStretch = false,
    this.onTapFunction,
  }) : super(key: key);

  final String content;
  final bool isDark;
  final Function onTapFunction;
  final doesStretch;

  @override
  Widget build(BuildContext context) {
    final double swidth = MediaQuery.of(context).size.width;

    return Expanded(
      flex: doesStretch ? 2 : 1,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          customBorder: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          splashColor: Colors.red,
          onTap: () => {onTapFunction()},
          child: Ink(
              height: swidth / 7,
              decoration: BoxDecoration(
                  color: isDark ? Colors.black : Colors.white,
                  border: Border.all(width: 2),
                  borderRadius: BorderRadius.circular(15)),
              child: Container(
                child: Center(
                    child: Text(
                  content,
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: isDark ? Colors.white : Colors.black),
                )),
              )),
        ),
      ),
    );
  }
}
