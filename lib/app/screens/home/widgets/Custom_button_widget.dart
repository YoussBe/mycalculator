import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key key,
    this.content,
    this.isDark = false,
    this.isBig = false,
    this.onTapButton,
  }) : super(key: key);

  final String content;
  final bool isDark;
  final bool isBig;
  final Function onTapButton;

  void tap() {}

  @override
  Widget build(BuildContext context) {
    final double maxWidth = MediaQuery.of(context).size.width;

    return Expanded(
      flex: isBig ? 2 : 1,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: () => onTapButton(),
          child: Container(
            decoration: BoxDecoration(
                color: isDark ? Colors.black : Colors.white,
                border: Border.all(width: 2),
                borderRadius: BorderRadius.circular(15)),
            height: maxWidth / 6,
            child: Center(
              child: Text(
                content,
                style: TextStyle(
                    color: isDark ? Colors.white : Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
