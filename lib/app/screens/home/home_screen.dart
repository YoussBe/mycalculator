import 'package:flutter/material.dart';
import 'package:function_tree/function_tree.dart';
<<<<<<< HEAD
import 'package:mycalculator/app/screens/home/widgets/Custom_button_widget.dart';
import 'package:mycalculator/app/screens/home/widgets/keyboard_widget.dart';
import 'package:photo_view/photo_view.dart';
=======
import 'package:mycalculator/app/modules/history/data/repository/history_repository.dart';
import 'package:mycalculator/app/screens/home/widgets/calcul_button.dart';
import 'package:mycalculator/app_routes.dart';
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
<<<<<<< HEAD
  void testFuntion(String symbol) {
    setState(() {
      equation = symbol;
    });
    print(equation);
  }

  String currentDouble = '';
  String equation = '';

  double result = 0;

  void addNumberToCurrentDouble(String number) {
    setState(() {
      currentDouble += number;
      result = double.parse(currentDouble);
    });
  }

  void addOperatorToEquation(String op) {
    setState(() {
      addNumberToList();
      addElementToEquation(op);
    });
  }

  void test(String op) {
    setState(() {
      addNumberToList();
      addElementToEquation(op);
    });
  }

  void addNumberToList() {
    setState(() {
      addElementToEquation(currentDouble);
      currentDouble = '';
    });
  }

  void addElementToEquation(String element) {
    setState(() {
      equation += element;
    });
  }

  void removeAll() {
    setState(() {
      result = 0;
      equation = '';
    });
    currentDouble = '';
  }

  void evaluateEquation() {
    addNumberToList();
    print(equation);
    result = equation.interpret().toDouble();
    print(result);
  }

  void testFun(String msg) {
    print(msg);
=======
  HistoryRepository historyRepository = HistoryRepository();
  List<String> equationElements = [];
  List<String> equationOperator = [];
  String fullEquation = '';
  double result = 0;
  String currentdouble = '';

  navigateNamedToHistory(context) {
    Navigator.pushNamed(context, kHistoryRoute);
  }

  void buildCurrentDouble(String digit) {
    setState(() {
      currentdouble += digit;
      result = double.parse(currentdouble);
    });
  }

  void addTofullEquation(String element) {
    fullEquation += element;
  }

  void addElementToEquation() {
    setState(() {
      equationElements.add(currentdouble);
      addTofullEquation(currentdouble);
      currentdouble = '';
    });
  }

  void remove() {
    setState(() {
      equationElements = [];
      equationOperator = [];
      fullEquation = '';
      result = 0;
      currentdouble = '';
    });
  }

  void addOperatorToEquation(String op) {
    print(equationElements);
    setState(() {
      equationOperator.add(op);
    });

    addElementToEquation();
    addTofullEquation(op);
  }

  void evaluateEquation() {
    setState(() {
      addElementToEquation();
      print(equationElements);

      result = fullEquation.interpret().toDouble();
    });
    saveReultInDB(result);
  }

  saveReultInCache(result) {
    historyRepository.saveResult(result);
  }

  saveReultInDB(result) {
    historyRepository.insertResult(result);
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
<<<<<<< HEAD
        elevation: 0,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 20),
            child: Icon(
              Icons.save,
              color: Colors.black,
            ),
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Container(
                  child: Text(equation),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  child: Container(
                    child: Text(
                      result.toString(),
                      style: TextStyle(
                          fontSize: 50,
                          height: 1,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                child: Column(
=======
        //elevation : effet ombre
        elevation: 0,
        //boutons d'action à droite de l'AppBar
        actions: [
          IconButton(
              icon: Icon(
                Icons.save,
                color: Colors.black,
              ),
              onPressed: () => navigateNamedToHistory(context))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Container(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Text(
                        fullEquation,
                        style: TextStyle(color: Colors.black45),
                        textAlign: TextAlign.end,
                      ),
                    ),
                    Text(
                      result.toString(),
                      style:
                          TextStyle(fontSize: 50, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
                Column(
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
<<<<<<< HEAD
                          CustomButton(
                            content: 'C',
                            onTapButton: (value) => removeAll(),
                          ),
                          CustomButton(
                            content: '(',
                            onTapButton: () => addOperatorToEquation('('),
                          ),
                          CustomButton(
                            content: ')',
                            onTapButton: () => addOperatorToEquation(')'),
                          ),
                          CustomButton(
                            content: '/',
                            onTapButton: () => addOperatorToEquation('/'),
=======
                          CalculButton(
                            content: 'C',
                            onTapFunction: () => remove(),
                          ),
                          CalculButton(
                            content: '(',
                            onTapFunction: () => addOperatorToEquation('('),
                          ),
                          CalculButton(
                            content: ')',
                            onTapFunction: () => addOperatorToEquation(')'),
                          ),
                          CalculButton(
                            content: '/',
                            onTapFunction: () => addOperatorToEquation('/'),
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
                          ),
                        ]),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
<<<<<<< HEAD
                          CustomButton(
                            content: '7',
                            isDark: true,
                            onTapButton: () => addNumberToCurrentDouble('7'),
                          ),
                          CustomButton(
                            content: '8',
                            isDark: true,
                            onTapButton: () => addNumberToCurrentDouble('8'),
                          ),
                          CustomButton(
                            content: '9',
                            isDark: true,
                            onTapButton: () => addNumberToCurrentDouble('9'),
                          ),
                          CustomButton(
                            content: 'X',
                            onTapButton: () => addOperatorToEquation('*'),
=======
                          CalculButton(
                            content: '7',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('7'),
                          ),
                          CalculButton(
                            content: '8',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('8'),
                          ),
                          CalculButton(
                            content: '9',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('9'),
                          ),
                          CalculButton(
                            content: 'X',
                            onTapFunction: () => addOperatorToEquation('*'),
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
                          ),
                        ]),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
<<<<<<< HEAD
                          CustomButton(
                            content: '4',
                            isDark: true,
                            onTapButton: () => addNumberToCurrentDouble('4'),
                          ),
                          CustomButton(
                            content: '5',
                            isDark: true,
                            onTapButton: () => addNumberToCurrentDouble('5'),
                          ),
                          CustomButton(
                            content: '6',
                            isDark: true,
                            onTapButton: () => addNumberToCurrentDouble('6'),
                          ),
                          CustomButton(
                            content: '-',
                            onTapButton: () => addOperatorToEquation('-'),
=======
                          CalculButton(
                            content: '4',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('4'),
                          ),
                          CalculButton(
                            content: '5',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('5'),
                          ),
                          CalculButton(
                            content: '6',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('6'),
                          ),
                          CalculButton(
                            content: '-',
                            onTapFunction: () => addOperatorToEquation('-'),
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
                          ),
                        ]),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
<<<<<<< HEAD
                          CustomButton(
                            content: '1',
                            onTapButton: () => addNumberToCurrentDouble('1'),
                            isDark: true,
                          ),
                          CustomButton(
                            content: '2',
                            onTapButton: () => addNumberToCurrentDouble('2'),
                            isDark: true,
                          ),
                          CustomButton(
                            content: '3',
                            onTapButton: () => addNumberToCurrentDouble('3'),
                            isDark: true,
                          ),
                          CustomButton(
                            content: '+',
                            onTapButton: () => addOperatorToEquation('+'),
=======
                          CalculButton(
                            content: '1',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('1'),
                          ),
                          CalculButton(
                            content: '2',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('2'),
                          ),
                          CalculButton(
                            content: '3',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('3'),
                          ),
                          CalculButton(
                            content: '+',
                            onTapFunction: () => addOperatorToEquation('+'),
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
                          ),
                        ]),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
<<<<<<< HEAD
                          CustomButton(
                            content: '0',
                            onTapButton: () => addNumberToCurrentDouble('0'),
                            isBig: true,
                            isDark: true,
                          ),
                          CustomButton(
                            content: '.',
                            onTapButton: () => addNumberToCurrentDouble('.'),
                            isDark: true,
                          ),
                          CustomButton(
                            content: '=',
                            onTapButton: () => evaluateEquation(),
                          ),
                        ])
                  ],
                ),
              ))
        ],
=======
                          CalculButton(
                            content: '0',
                            isDark: true,
                            doesStretch: true,
                            onTapFunction: () => buildCurrentDouble('0'),
                          ),
                          CalculButton(
                            content: '.',
                            isDark: true,
                            onTapFunction: () => buildCurrentDouble('.'),
                          ),
                          CalculButton(
                            content: '=',
                            isDark: true,
                            onTapFunction: () => evaluateEquation(),
                          ),
                        ]),
                  ],
                )
              ]),
        ),
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
      ),
    );
  }
}
