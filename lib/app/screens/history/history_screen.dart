import 'package:flutter/material.dart';
import 'package:mycalculator/app/modules/history/data/repository/history_repository.dart';
import 'package:mycalculator/app/modules/history/model/history_model.dart';
import 'package:mycalculator/app_routes.dart';

class HistoryScreen extends StatefulWidget {
  HistoryScreen({Key key}) : super(key: key);

  @override
  _HistoryScreenState createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  HistoryRepository historyRepository = HistoryRepository();

  String lastResult = '';
  List<History> lastResults = [];

  retrieveLastResult() async {
    lastResult = await historyRepository.retrieve();
    setState(() {
      lastResult = lastResult;
    });
  }

  queryResults() async {
    lastResults = await historyRepository.loadResultsFromDb();
    setState(() {
      lastResults = lastResults;
    });
  }

  @override
  void initState() {
    queryResults();
    super.initState();
  }

  final List<Map<dynamic, String>> historyList = [
    {'title': 'Total achats', 'date': '3 mars 2O21', 'result': '299'},
    {'title': 'Mes dépenses', 'date': '1 mars 2O21', 'result': '1990.90'},
    {'title': 'Mes dépenses', 'date': '1 mars 2O21', 'result': '1990.90'},
    {'title': 'Mes dépenses', 'date': '1 mars 2O21', 'result': '1990.90'},
    {'title': 'Mes dépenses', 'date': '1 mars 2O21', 'result': '1990.90'},
    {'title': 'Mes dépenses', 'date': '1 mars 2O21', 'result': '1990.90'}
  ];

  void navigateNamedToHome(context) {
    Navigator.pushNamed(context, kHomeRoute);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
            'Calcul récents',
            style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          //elevation : effet ombre
          elevation: 0,
          //boutons d'action à droite de l'AppBar
          actions: [
            IconButton(
                icon: Icon(
                  Icons.format_list_numbered,
                  color: Colors.black,
                ),
                onPressed: () => Navigator.pop(context))
          ],
        ),
        body: ListView(
            children: lastResults
                .map(
                  (element) => Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            Text(
                              element.title,
                              style: TextStyle(fontSize: 24),
                            ),
                            Text(
                              element.date,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.black.withOpacity(0.7)),
                            ),
                            Text(
                              element.result.toString(),
                              style: TextStyle(
                                  fontSize: 35, fontWeight: FontWeight.w900),
                            )
                          ]),
                    ),
                  ),
                )
                .toList()));
  }
}
