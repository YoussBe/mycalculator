import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
<<<<<<< HEAD
=======
import 'package:mycalculator/app/screens/home/home_screen.dart';
import 'package:mycalculator/app_routes.dart';
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

<<<<<<< HEAD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.amber.withOpacity(0.2),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Center(
              child: Padding(
            padding: const EdgeInsets.all(50),
            child: Image.asset(
              ('assets/images/logo.jpg'),
              height: 120,
            ),
          )),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 60),
            //EdgeInsets.symetric(),
            //EdgeInsets.only(),

            child: Text(
              'Ma Calculatrice',
              style: TextStyle(
                  fontSize: 50, height: 1, fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
          )
        ]));
=======
  navigateToHome(context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
    );
  }

  navigateNamedToHome(context) {
    Navigator.pushNamedAndRemoveUntil(context, kHomeRoute, (_) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // le scaffold est un widget spécial primordial pour l'utilisation des widgets dans chaque écran
      body: Container(
        child: Column(
          //le column sert à organiser les widgets fils verticaelement
          mainAxisAlignment: MainAxisAlignment.center,
          //ici les fils sont centrer veriticalement
          children: [
            Padding(
              //le padding crée un espace autour du widget
              padding: const EdgeInsets.all(8.0),
              child: Center(
                //le widget image est un widget qui affiche les images depuis l'application ou depuis un network
                child: InkWell(
                  onTap: () => navigateNamedToHome(context),
                  child: Image.asset(
                    'assets/images/logo.png',
                    height: 120,
                  ),
                ),
              ),
            ),
            Padding(
              // un padding peut avoir plusieurs parametre : exemple ici le padding est seulement horizontale
              padding: const EdgeInsets.symmetric(horizontal: 60),
              child: Text(
                'Ma Calculatrice',
                style: TextStyle(
                    fontSize: 30, height: 1, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
>>>>>>> 27df6211d21455f7db4960c06697c0d3142d268a
  }
}
