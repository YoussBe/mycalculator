import 'package:mycalculator/app/modules/history/data/provider/history_cache_provider.dart';
import 'package:mycalculator/app/modules/history/data/provider/history_db_provider.dart';
import 'package:mycalculator/app/modules/history/model/history_model.dart';

class HistoryRepository {
  final _historyCache = HistoryCacheProvider();
  final dbProvider = HistoryDbProvider.instance;

  Future saveResult(double result) async {
    await _historyCache.saveLastResul(result);
  }

  Future<String> retrieve() async {
    return await _historyCache.retrieveLastResult();
  }

  Future insertResult(result) async {
    final date = DateTime.now();
    final History history = new History(
        result: result,
        date: date.day.toString() +
            '-' +
            date.month.toString() +
            '-' +
            date.year.toString());
    await dbProvider.insert(history.toJson());
  }

  Future<List<History>> loadResultsFromDb() async {
    List<History> videos = [];

    var query = await dbProvider.queryAllRows();
    videos = query.map((e) => History.fromJson(e)).toList();

    return videos;
  }

  Future updateVideo(History video) async {
    await dbProvider.update(video.toJson());
  }
}
