class History {
  final int id;
  final String title;
  final String date;
  double result;

  History({this.id, this.title = '', this.date = '', this.result});

  factory History.fromJson(Map<String, dynamic> json) {
    var video = new History(
        id: json['_id'],
        title: json['title'],
        date: json['date'],
        result: json['result']);
    return video;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['_id'] = this.id;
    data['title'] = this.title;
    data['date'] = this.date;
    data['result'] = this.result.toString();
    return data;
  }
}
